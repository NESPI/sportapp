package com.tacotimes.sportapp

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.core.splashscreen.SplashScreen.Companion.installSplashScreen
import com.tacotimes.sportapp.presentation.leagues.LeaguesRoute
import com.tacotimes.sportapp.presentation.ui.theme.SportAppTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
internal class MainActivity : ComponentActivity() {
    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        installSplashScreen().apply { setKeepOnScreenCondition { viewModel.uiState.value.isLoading } }

        setContent {
            val uiState by viewModel.uiState.collectAsState()

            if (uiState.errorMessage != null) {
                Toast.makeText(this, uiState.errorMessage, Toast.LENGTH_LONG).show()
                finish()
            } else {
                SportAppTheme {
                    LeaguesRoute()
                }
            }
        }
    }
}