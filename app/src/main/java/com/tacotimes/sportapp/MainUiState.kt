package com.tacotimes.sportapp

internal data class MainUiState(
    val isLoading: Boolean = false,
    val errorMessage: String? = null,
)