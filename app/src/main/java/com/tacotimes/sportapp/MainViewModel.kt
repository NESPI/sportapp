package com.tacotimes.sportapp

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tacotimes.core.domain.repository.SportRepository
import com.tacotimes.core.platform.functional.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
internal class MainViewModel @Inject constructor(
    private val sportRepository: SportRepository
) : ViewModel() {
    private val _uiState = MutableStateFlow(MainUiState())
    val uiState = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            sportRepository.getLeagues(fetchFromRemote = true, query = "").collect { resource ->
                when (resource) {
                    is Resource.Error -> _uiState.update {
                        it.copy(isLoading = false, errorMessage = resource.message)
                    }

                    is Resource.Loading -> _uiState.update {
                        it.copy(isLoading = resource.isLoading, errorMessage = null)
                    }

                    is Resource.Success -> _uiState.update {
                        it.copy(isLoading = false, errorMessage = null)
                    }
                }
            }
        }
    }
}