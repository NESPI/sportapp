package com.tacotimes.sportapp.presentation.leagues

internal sealed class LeaguesAction {
    object Refresh : LeaguesAction()
    data class OnSearchQueryChange(val searchQuery: String) : LeaguesAction()
}