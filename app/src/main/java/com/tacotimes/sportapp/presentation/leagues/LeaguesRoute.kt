package com.tacotimes.sportapp.presentation.leagues

import android.content.res.Configuration
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.hilt.navigation.compose.hiltViewModel
import coil.compose.AsyncImage
import coil.request.CachePolicy
import coil.request.ImageRequest
import com.airbnb.lottie.compose.LottieAnimation
import com.airbnb.lottie.compose.LottieCompositionSpec
import com.airbnb.lottie.compose.LottieConstants
import com.airbnb.lottie.compose.rememberLottieComposition
import com.google.accompanist.swiperefresh.SwipeRefresh
import com.google.accompanist.swiperefresh.rememberSwipeRefreshState
import com.tacotimes.core.domain.models.League
import com.tacotimes.sportapp.R
import com.tacotimes.sportapp.presentation.ui.composables.CustomTextField
import com.tacotimes.sportapp.presentation.ui.theme.SpaceLarge
import com.tacotimes.sportapp.presentation.ui.theme.SpaceLargeX
import com.tacotimes.sportapp.presentation.ui.theme.SpaceMedium
import com.tacotimes.sportapp.presentation.ui.theme.SpaceSmall
import com.tacotimes.sportapp.presentation.ui.theme.SportAppTheme
import com.tacotimes.sportapp.presentation.webview_sport.SportWebViewScreen

@Composable
internal fun LeaguesRoute(
    viewModel: LeaguesViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsState()
    var isOpenWebView by remember { mutableStateOf(false) }

    LeaguesScreen(
        uiState = uiState,
        onAction = viewModel::onEvent,
        onSelect = { isOpenWebView = true }
    )

    if (isOpenWebView) {
        SportWebViewScreen()
    }
}

@Composable
private fun LeaguesScreen(
    uiState: LeaguesUiState,
    onAction: (LeaguesAction) -> Unit,
    onSelect: () -> Unit
) {
    val swipeRefreshState = rememberSwipeRefreshState(
        isRefreshing = uiState.isRefreshing
    )

    SwipeRefresh(
        state = swipeRefreshState,
        onRefresh = { onAction(LeaguesAction.Refresh) },
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .fillMaxSize()
                .padding(SpaceMedium)
        ) {
            val leaguesGroupEventDate = uiState.leagues.groupBy { it.eventDate }

            Header()

            Spacer(modifier = Modifier.height(SpaceLarge))
            Leagues(uiState.leagues)

            Spacer(modifier = Modifier.height(SpaceLarge))
            Search(uiState, onAction)

            Spacer(modifier = Modifier.height(SpaceLargeX))
            Matchies(
                leaguesGroupEventDate = leaguesGroupEventDate,
                onSelect = onSelect
            )
        }
    }
}


@Composable
private fun Header() {
    Text(
        text = stringResource(id = R.string.matches),
        style = MaterialTheme.typography.headlineMedium,
        modifier = Modifier.fillMaxWidth(),
        fontWeight = FontWeight(600),
    )
}

@Composable
private fun Leagues(leagues: List<League>) {
    if (leagues.isEmpty()) {
        Text(
            text = stringResource(id = R.string.not_found),
            style = TextStyle(
                fontSize = 20.sp,
                fontWeight = FontWeight(700),
                color = Color(0xFF150000),
            )
        )
    } else {
        LazyRow(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.spacedBy(SpaceLarge),
        ) {
            items(leagues) { league ->
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier.width(80.dp)
                ) {
                    AsyncImage(
                        model = ImageRequest.Builder(LocalContext.current.applicationContext)
                            .data(league.eventHomeTeamLogo)
                            .crossfade(true)
                            .diskCachePolicy(CachePolicy.ENABLED)
                            .setHeader("Cache-Control", "max-age=31536000")
                            .error(R.drawable.round_sports_handball_24)
                            .build(),
                        contentDescription = null,
                        modifier = Modifier
                            .border(width = 1.5.dp, color = Color(0xFFEEEDED))
                            .size(80.dp)
                            .padding(SpaceMedium),
                    )

                    Text(
                        text = league.leagueName,
                        textAlign = TextAlign.Center,
                        fontWeight = FontWeight(600),
                        maxLines = 2,
                        modifier = Modifier
                            .padding(top = SpaceMedium)
                    )
                }
            }
        }
    }
}

@Composable
private fun Search(
    uiState: LeaguesUiState,
    onAction: (LeaguesAction) -> Unit
) {
    CustomTextField(
        value = uiState.searchQuery,
        onValueChange = {
            onAction(LeaguesAction.OnSearchQueryChange(it))
        },
        modifier = Modifier
            .fillMaxWidth()
            .background(Color.White),
        placeholder = stringResource(id = R.string.search),
        maxLines = 1,
    )
}

@Composable
@OptIn(ExperimentalFoundationApi::class)
private fun Matchies(
    leaguesGroupEventDate: Map<String, List<League>>,
    onSelect: () -> Unit
) {
    if (leaguesGroupEventDate.isEmpty()) {
        NotFound()
    } else {
        LazyColumn(
            modifier = Modifier.fillMaxSize(1f)
        ) {
            leaguesGroupEventDate.forEach { (name, items) ->
                stickyHeader {
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                            .background(Color.White)
                            .padding(SpaceMedium)
                    ) {
                        Text(
                            text = name,
                            style = TextStyle(
                                fontSize = 16.sp,
                                fontWeight = FontWeight(600),
                                color = Color(0xFF322D2D),
                            )
                        )
                    }
                }

                itemsIndexed(items) { _, item ->
                    Column(
                        modifier = Modifier
                            .fillMaxSize()
                            .clickable { onSelect() },
                    ) {
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(150.dp)
                                .padding(SpaceMedium)
                        ) {
                            Row(modifier = Modifier.fillMaxWidth()) {
                                Text(
                                    text = item.eventHomeTeam,
                                    modifier = Modifier.weight(1f),
                                    textAlign = TextAlign.Start,
                                    maxLines = 2,
                                    fontWeight = FontWeight(600),
                                )
                                Text(
                                    text = item.eventStatus,
                                    modifier = Modifier.weight(1f),
                                    textAlign = TextAlign.End,
                                    fontWeight = FontWeight(600),
                                )
                            }

                            Spacer(modifier = Modifier.height(SpaceSmall))
                            Text(
                                text = String.format(
                                    stringResource(id = R.string.league),
                                    item.leagueName
                                ),
                            )
                            Spacer(modifier = Modifier.height(SpaceSmall))
                            Text(
                                text = String.format(
                                    stringResource(id = R.string.season),
                                    item.leagueSeason
                                ),
                            )
                        }

                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .height(55.dp)
                                .background(Color(0xFF1E1E1E))
                                .padding(SpaceMedium)
                        ) {
                            Text(
                                text = String.format(
                                    stringResource(id = R.string.final_result),
                                    item.eventFinalResult
                                ),
                                color = Color.White,
                                fontWeight = FontWeight(600),
                            )
                        }
                    }
                }
            }
        }
    }
}

@Composable
private fun NotFound() {
    val loadingAnimationComposition by rememberLottieComposition(
        spec = LottieCompositionSpec.RawRes(R.raw.not_found)
    )

    LottieAnimation(
        modifier = Modifier.fillMaxSize(),
        composition = loadingAnimationComposition,
        iterations = LottieConstants.IterateForever,
        alignment = Alignment.Center
    )
}

@Preview("default", showBackground = true)
@Preview("dark theme", showBackground = true, uiMode = Configuration.UI_MODE_NIGHT_YES)
@Composable
private fun LeaguesScreenPreview() {
    SportAppTheme {
        LeaguesScreen(
            uiState = LeaguesUiState(
                searchQuery = "",
                leagues = listOf(
                    League(
                        eventKey = 90371,
                        eventDate = "2023-05-23",
                        eventTime = "18:00",
                        eventHomeTeam = "Hapoel Tel-Aviv",
                        homeTeamKey = 446,
                        eventAwayTeam = "Nes Ziona",
                        awayTeamKey = 442,
                        eventFinalResult = "102 - 69",
                        eventQuarter = "",
                        eventStatus = "Finished",
                        countryName = "Israel",
                        leagueName = "Super League",
                        leagueKey = 763,
                        leagueRound = null,
                        leagueSeason = "2022/2023",
                        eventLive = "0",
                        eventHomeTeamLogo = "https://apiv2.allsportsapi.com/logo-basketball/446_hapoel_tel-aviv.jpg",
                        eventAwayTeamLogo = "https://apiv2.allsportsapi.com/logo-basketball/442_nes_ziona.jpg"
                    ),
                    League(
                        eventKey = 90371,
                        eventDate = "2023-05-23",
                        eventTime = "18:00",
                        eventHomeTeam = "Hapoel Tel-Aviv",
                        homeTeamKey = 446,
                        eventAwayTeam = "Nes Ziona",
                        awayTeamKey = 442,
                        eventFinalResult = "102 - 69",
                        eventQuarter = "",
                        eventStatus = "Finished",
                        countryName = "Israel",
                        leagueName = "Super League",
                        leagueKey = 763,
                        leagueRound = null,
                        leagueSeason = "2022/2023",
                        eventLive = "0",
                        eventHomeTeamLogo = "https://apiv2.allsportsapi.com/logo-basketball/446_hapoel_tel-aviv.jpg",
                        eventAwayTeamLogo = "https://apiv2.allsportsapi.com/logo-basketball/442_nes_ziona.jpg"
                    )
                ),
            ),
            onAction = {},
            onSelect = {},
        )
    }
}