package com.tacotimes.sportapp.presentation.leagues

import com.tacotimes.core.domain.models.League

internal data class LeaguesUiState(
    val leagues: List<League> = emptyList(),
    val searchQuery: String = "",
    val isLoading: Boolean = false,
    val isRefreshing: Boolean = false,
)