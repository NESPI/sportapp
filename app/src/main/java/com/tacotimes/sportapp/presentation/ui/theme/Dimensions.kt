package com.tacotimes.sportapp.presentation.ui.theme

import androidx.compose.ui.unit.dp

val SpaceSmall = 8.dp
val SpaceMedium = 16.dp
val SpaceLarge = 24.dp
val SpaceLargeX = 32.dp