package com.tacotimes.sportapp.presentation.leagues

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tacotimes.core.domain.repository.SportRepository
import com.tacotimes.core.platform.functional.Resource
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.time.Duration.Companion.milliseconds

@HiltViewModel
internal class LeaguesViewModel @Inject constructor(
    private val leagueRepository: SportRepository
) : ViewModel() {
    private val _uiState = MutableStateFlow(LeaguesUiState())
    val uiState = _uiState.asStateFlow()

    private var searchJob: Job? = null

    init {
        getLeagues()
    }

    fun onEvent(action: LeaguesAction) {
        when (action) {
            is LeaguesAction.Refresh -> {
                getLeagues(fetchFromRemote = true)
            }

            is LeaguesAction.OnSearchQueryChange -> {
               _uiState.update { it.copy(searchQuery = action.searchQuery) }
                searchJob?.cancel()
                searchJob = viewModelScope.launch {
                    delay(5.milliseconds)
                    getLeagues()
                }
            }
        }
    }

    private fun getLeagues(
        query: String = _uiState.value.searchQuery.lowercase(),
        fetchFromRemote: Boolean = false
    ) {
        viewModelScope.launch {
            leagueRepository
                .getLeagues(fetchFromRemote, query)
                .collect { result ->
                    when (result) {
                        is Resource.Success -> {
                            result.data?.let { leagues ->
                                _uiState.update { it.copy(leagues = leagues, isLoading = false) }
                            }
                        }

                        is Resource.Error -> Unit
                        is Resource.Loading -> _uiState.update { it.copy(isLoading = true) }
                    }
                }
        }
    }
}