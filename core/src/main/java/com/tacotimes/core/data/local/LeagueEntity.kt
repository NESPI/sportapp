package com.tacotimes.core.data.local

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class LeagueEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int = 0,

    @ColumnInfo(name = "event_key") val eventKey: Int,
    @ColumnInfo(name = "event_date") val eventDate: String,
    @ColumnInfo(name = "event_time") val eventTime: String,
    @ColumnInfo(name = "event_home_team") val eventHomeTeam: String,
    @ColumnInfo(name = "home_team_key") val homeTeamKey: Int,
    @ColumnInfo(name = "event_away_team") val eventAwayTeam: String,
    @ColumnInfo(name = "away_team_key") val awayTeamKey: Int,
    @ColumnInfo(name = "event_final_result") val eventFinalResult: String,
    @ColumnInfo(name = "event_quarter") val eventQuarter: String?,
    @ColumnInfo(name = "event_status") val eventStatus: String,
    @ColumnInfo(name = "country_name") val countryName: String,
    @ColumnInfo(name = "league_name") val leagueName: String,
    @ColumnInfo(name = "league_key") val leagueKey: Int,
    @ColumnInfo(name = "league_round") val leagueRound: String?,
    @ColumnInfo(name = "league_season") val leagueSeason: String,
    @ColumnInfo(name = "event_live") val eventLive: String,
    @ColumnInfo(name = "event_home_team_logo") val eventHomeTeamLogo: String?,
    @ColumnInfo(name = "event_away_team_logo") val eventAwayTeamLogo: String?,
)