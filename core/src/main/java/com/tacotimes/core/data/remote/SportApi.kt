package com.tacotimes.core.data.remote

import com.tacotimes.core.data.remote.dto.LeaguesDto
import retrofit2.Response
import retrofit2.http.POST
import retrofit2.http.Query

internal interface SportApi {
    companion object {
        const val API_KEY = "b23ef7b0a18133c4ab68a4dc8d86f54ef8bc3f8e659bf5eaa7c8adae654a7c48"
        const val BASE_URL = "https://apiv2.allsportsapi.com"
    }

    @POST("basketball")
    suspend fun getBasketball(
        @Query("met") met: String = "Fixtures",
        @Query("APIkey") apiKey: String = API_KEY,
        @Query("from") from: String = "2023-05-23",
        @Query("to") to: String = "2023-05-23",
    ): Response<LeaguesDto>
}