package com.tacotimes.core.data.remote.dto

import com.google.gson.annotations.SerializedName

internal data class LeaguesDto(
    @SerializedName("success") override val status: StatusType,
    @SerializedName("result") val result: List<LeagueDto>,
) : BaseDto(status)