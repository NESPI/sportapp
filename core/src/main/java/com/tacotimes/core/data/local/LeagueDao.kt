package com.tacotimes.core.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
internal interface LeagueDao {
    @Query(
        """
            SELECT * FROM LeagueEntity WHERE LOWER(event_home_team) LIKE '%' || LOWER(:query) || '%'
        """
    )
    suspend fun search(query: String): List<LeagueEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertList(entities: List<LeagueEntity>)

    @Query("DELETE FROM LeagueEntity")
    suspend fun clear()
}