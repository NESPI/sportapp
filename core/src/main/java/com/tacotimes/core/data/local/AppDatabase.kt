package com.tacotimes.core.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.tacotimes.core.data.local.AppDatabase.Companion.DATABASE_VERSION

@Database(
    entities = [LeagueEntity::class],
    version = DATABASE_VERSION
)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun leagueDao(): LeagueDao

    companion object {
        const val DATA_BASE_NAME = "sport.db"
        const val DATABASE_VERSION = 1
    }
}