package com.tacotimes.core.data.remote.dto

internal open class BaseDto(
    open val status: StatusType
)