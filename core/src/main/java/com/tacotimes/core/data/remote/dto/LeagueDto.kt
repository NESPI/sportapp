package com.tacotimes.core.data.remote.dto

import com.google.gson.annotations.SerializedName

data class LeagueDto(
    @SerializedName("event_key") val eventKey: Int,
    @SerializedName("event_date") val eventDate: String,
    @SerializedName("event_time") val eventTime: String,
    @SerializedName("event_home_team") val eventHomeTeam: String,
    @SerializedName("home_team_key") val homeTeamKey: Int,
    @SerializedName("event_away_team") val eventAwayTeam: String,
    @SerializedName("away_team_key") val awayTeamKey: Int,
    @SerializedName("event_final_result") val eventFinalResult: String,
    @SerializedName("event_quarter") val eventQuarter: String?,
    @SerializedName("event_status") val eventStatus: String,
    @SerializedName("country_name") val countryName: String,
    @SerializedName("league_name") val leagueName: String,
    @SerializedName("league_key") val leagueKey: Int,
    @SerializedName("league_round") val leagueRound: String?,
    @SerializedName("league_season") val leagueSeason: String,
    @SerializedName("event_live") val eventLive: String,
    @SerializedName("event_home_team_logo") val eventHomeTeamLogo: String?,
    @SerializedName("event_away_team_logo") val eventAwayTeamLogo: String?,
)