package com.tacotimes.core.data.mapper

import com.tacotimes.core.data.local.LeagueEntity
import com.tacotimes.core.data.remote.dto.LeagueDto
import com.tacotimes.core.domain.models.League

fun LeagueEntity.toLeague(): League {
    return League(
        eventKey = this.eventKey,
        eventDate = this.eventDate,
        eventTime = this.eventTime,
        eventHomeTeam = this.eventHomeTeam,
        homeTeamKey = this.homeTeamKey,
        eventAwayTeam = this.eventAwayTeam,
        awayTeamKey = this.awayTeamKey,
        eventFinalResult = this.eventFinalResult,
        eventQuarter = this.eventQuarter,
        eventStatus = this.eventStatus,
        countryName = this.countryName,
        leagueName = this.leagueName,
        leagueKey = this.leagueKey,
        leagueRound = this.leagueRound,
        leagueSeason = this.leagueSeason,
        eventLive = this.eventLive,
        eventHomeTeamLogo = this.eventHomeTeamLogo,
        eventAwayTeamLogo = this.eventAwayTeamLogo,
    )
}

fun LeagueDto.toLeague(): League {
    return League(
        eventKey = this.eventKey,
        eventDate = this.eventDate,
        eventTime = this.eventTime,
        eventHomeTeam = this.eventHomeTeam,
        homeTeamKey = this.homeTeamKey,
        eventAwayTeam = this.eventAwayTeam,
        awayTeamKey = this.awayTeamKey,
        eventFinalResult = this.eventFinalResult,
        eventQuarter = this.eventQuarter,
        eventStatus = this.eventStatus,
        countryName = this.countryName,
        leagueName = this.leagueName,
        leagueKey = this.leagueKey,
        leagueRound = this.leagueRound,
        leagueSeason = this.leagueSeason,
        eventLive = this.eventLive,
        eventHomeTeamLogo = this.eventHomeTeamLogo,
        eventAwayTeamLogo = this.eventAwayTeamLogo,
    )
}

fun LeagueDto.toLeagueEntity(): LeagueEntity {
    return LeagueEntity(
        eventKey = this.eventKey,
        eventDate = this.eventDate,
        eventTime = this.eventTime,
        eventHomeTeam = this.eventHomeTeam,
        homeTeamKey = this.homeTeamKey,
        eventAwayTeam = this.eventAwayTeam,
        awayTeamKey = this.awayTeamKey,
        eventFinalResult = this.eventFinalResult,
        eventQuarter = this.eventQuarter,
        eventStatus = this.eventStatus,
        countryName = this.countryName,
        leagueName = this.leagueName,
        leagueKey = this.leagueKey,
        leagueRound = this.leagueRound,
        leagueSeason = this.leagueSeason,
        eventLive = this.eventLive,
        eventHomeTeamLogo = this.eventHomeTeamLogo,
        eventAwayTeamLogo = this.eventAwayTeamLogo,
    )
}