package com.tacotimes.core.data.remote.dto

import com.google.gson.annotations.SerializedName

enum class StatusType {
    @SerializedName("0")
    FAILURE,

    @SerializedName("1")
    SUCCESS
}