package com.tacotimes.core.data.repository

import com.tacotimes.core.data.local.LeagueDao
import com.tacotimes.core.data.mapper.toLeague
import com.tacotimes.core.data.mapper.toLeagueEntity
import com.tacotimes.core.data.remote.SportApi
import com.tacotimes.core.data.remote.dto.StatusType
import com.tacotimes.core.domain.models.League
import com.tacotimes.core.domain.repository.SportRepository
import com.tacotimes.core.platform.functional.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject

internal class SportRepositoryImpl @Inject constructor(
    private val leagueDao: LeagueDao,
    private val api: SportApi,
) : SportRepository {
    override suspend fun getLeagues(
        fetchFromRemote: Boolean,
        query: String
    ): Flow<Resource<List<League>>> = flow {
        emit(Resource.Loading(true))

        val localListings = leagueDao.search(query)
        val isDbEmpty = localListings.isEmpty() && query.isBlank()
        val shouldLoadFromCache = !isDbEmpty && !fetchFromRemote
        if (shouldLoadFromCache) {
            emit(Resource.Success(
                data = localListings.map { it.toLeague() }
            ))
            return@flow
        }

        val remote = try {
            val response = api.getBasketball()
            val result = response.body()
            if (result != null && response.isSuccessful && result.status == StatusType.SUCCESS) {
                result
            } else {
                emit(Resource.Error("Couldn't load data"))
                null
            }
        } catch (e: IOException) {
            e.printStackTrace()
            emit(Resource.Error("Couldn't load data"))
            null
        } catch (e: HttpException) {
            e.printStackTrace()
            emit(Resource.Error("Couldn't load data"))
            null
        }

        remote?.let { leagues ->
            leagueDao.clear()
            leagueDao.insertList(
                leagues.result.map { it.toLeagueEntity() }
            )
            emit(Resource.Success(
                data = leagueDao
                    .search("")
                    .map { it.toLeague() }
            ))
            emit(Resource.Loading(false))
        }
    }
}