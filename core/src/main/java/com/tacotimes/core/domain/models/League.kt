package com.tacotimes.core.domain.models

data class League(
    val eventKey: Int,
    val eventDate: String,
    val eventTime: String,
    val eventHomeTeam: String,
    val homeTeamKey: Int,
    val eventAwayTeam: String,
    val awayTeamKey: Int,
    val eventFinalResult: String,
    val eventQuarter: String?,
    val eventStatus: String,
    val countryName: String,
    val leagueName: String,
    val leagueKey: Int,
    val leagueRound: String?,
    val leagueSeason: String,
    val eventLive: String,
    val eventHomeTeamLogo: String?,
    val eventAwayTeamLogo: String?
)