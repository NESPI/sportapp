package com.tacotimes.core.domain.repository

import com.tacotimes.core.domain.models.League
import com.tacotimes.core.platform.functional.Resource
import kotlinx.coroutines.flow.Flow

interface SportRepository {
    suspend fun getLeagues(
        fetchFromRemote: Boolean,
        query: String
    ): Flow<Resource<List<League>>>
}