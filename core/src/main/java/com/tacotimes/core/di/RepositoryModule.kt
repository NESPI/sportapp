package com.tacotimes.core.di

import com.tacotimes.core.data.repository.SportRepositoryImpl
import com.tacotimes.core.domain.repository.SportRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
internal abstract class RepositoryModule {

    @Binds
    @Singleton
    abstract fun bindStockRepository(
        sportRepositoryImpl: SportRepositoryImpl
    ): SportRepository
}