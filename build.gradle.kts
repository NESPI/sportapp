buildscript {
    dependencies {
        classpath("com.google.gms:google-services:4.3.15")
    }
}
plugins {
    id(Plugins.ANDROID_APPLICATION) version (PluginVersion.AGP) apply false
    id(Plugins.ANDROID_KOTLIN) version (PluginVersion.ANDROID_LIBRARY) apply false
    id(Plugins.DAGGER_HILT) version (PluginVersion.DAGGER_VERSION) apply false
}