/* Module Level */
object Plugins {
	const val ANDROID_APPLICATION = "com.android.application"
	const val ANDROID = "android"
	const val KAPT = "kotlin-kapt"
	const val ANDROID_LIBRARY = "com.android.library"
	const val ANDROID_KOTLIN = "org.jetbrains.kotlin.android"
	const val KOTLIN_PARCELIZE = "kotlin-parcelize"

	const val DAGGER_HILT_PLUGIN = "dagger.hilt.android.plugin"
	const val DAGGER_HILT = "com.google.dagger.hilt.android"
	const val GOOGLE_SERVICES = "com.google.gms:google-services"
	const val GOOGLE_SERVICES_ID = "com.google.gms.google-services"
}

object PluginVersion {
	const val AGP = "7.4.2"
	const val ANDROID_LIBRARY = "1.8.10"
	const val DAGGER_VERSION = "2.45"
	const val GOOGLE_SERVICES_VERSION = "4.3.15"
}