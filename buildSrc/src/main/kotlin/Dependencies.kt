object Deps {
    object Compose {
        const val composeVersion = "1.1.1"
        const val composeCompilerVersion = "1.2.0-alpha07"
        const val material = "androidx.compose.material:material:$composeVersion"
        const val compiler = "androidx.compose.compiler:compiler:$composeCompilerVersion"
    }

    object SplashScreen {
        private const val version = "1.0.0-rc01"
        const val splashAndroidApi = "androidx.core:core-splashscreen:$version"
    }

    object Lottie {
        private const val version = "6.0.0"
        const val compose = "com.airbnb.android:lottie-compose:$version"
    }

    object Dagger {
        private const val version = "2.45"
        const val hiltAndroid = "com.google.dagger:hilt-android:$version"
        const val hiltAndroidCompiler = "com.google.dagger:hilt-compiler:$version"
    }

    object Room {
        private const val version = "2.4.0"
        const val runtime = "androidx.room:room-runtime:$version"
        const val ktx = "androidx.room:room-ktx:$version"
        const val compiler = "androidx.room:room-compiler:$version"
    }

    object Retrofit {
        private const val version = "2.9.0"
        const val retrofit = "com.squareup.retrofit2:retrofit:$version"
        const val gsonConverter = "com.squareup.retrofit2:converter-gson:$version"
    }

    object Gson {
        private const val version = "2.10.1"
        const val gson = "com.google.code.gson:gson:$version"
    }

    object Coil {
        private const val version = "2.4.0"
        const val coil = "io.coil-kt:coil-compose:$version"
    }
}

object TestDeps {
    object AndroidX {
        private const val version = "1.4.0"

        // AndroidX Test - JVM Testing
        const val coreKtx = "androidx.test:core-ktx:$version"
        const val rules = "androidx.test:rules:$version"
        const val coreTesting = "androidx.arch.core:core-testing:2.1.0"
        const val androidX_jUnit = "androidx.test.ext:junit-ktx:1.1.3"
        const val navigationTest =
            "androidx.navigation:navigation-testing:2.4.2"
    }

    object Coroutines {
        private const val version = "1.3.7"
        const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object JUnit {
        private const val version = "4.13.2"
        const val junit = "junit:junit:$version"
    }

    object MockWebServer {
        private const val version = "4.9.3"
        const val mockwebserver = "com.squareup.okhttp3:mockwebserver:$version"
        const val okhttpIdlingResource = "com.jakewharton.espresso:okhttp3-idling-resource:1.0.0"
    }

    object MockK {
        const val mockK = "io.mockk:mockk:1.10.0"
    }

    object Mockito {
        private const val version = "4.3.0"
        const val core = "org.mockito:mockito-core:$version"
        const val inline = "org.mockito:mockito-inline:$version"
        const val android = "org.mockito:mockito-android:$version"
    }

    object RoboElectric {
        private const val version = "4.6"
        const val robolectric = "org.robolectric:robolectric:$version"
    }

    object Turbine {
        private const val version = "0.7.0"
        const val turbine = "app.cash.turbine:turbine:$version"
    }

    const val truth = "com.google.truth:truth:1.0.1"
    const val espressoCore = "androidx.test.espresso:espresso-core:3.4.0"
}
