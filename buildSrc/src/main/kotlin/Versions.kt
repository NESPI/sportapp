object AndroidConfig {
    const val APPLICATION_ID = "com.tacotimes.sportapp"
    const val MIN_SDK = 26
    const val TARGET_SDK = 34
    const val COMPILE_SDK = 34

    private const val versionMajor = 1
    private const val versionMinor = 0
    private const val versionPatch = 0

    const val VERSION_CODE = 1
    const val VERSION_NAME = "$versionMajor.$versionMinor.$versionPatch"

    const val TEST_INSTRUMENTATION_RUNNER = "androidx.test.runner.AndroidJUnitRunner"

    private const val projectStartTimeMillis = 1517443200000
    val versionBuild = ((System.currentTimeMillis() - projectStartTimeMillis) / 6000).toInt()
}