object Modules {
    const val app = ":app"
    const val core = ":core"
    const val core_ui = ":core-ui"
}